import java.util.*;
import java.util.stream.Collectors;

public class WordFrequencyGame {

    private static String ERROR_MESSAGE = "Calculate Error";
    private static int INIT_COUNT = 1;

    public String getResult(String inputStr) {
        try {
            List<WordFrequency> wordFrequencyList = getWordFrequencyList(inputStr);
            Map<String, List<WordFrequency>> wordFrequencyListMap = getWordFrequencyListMap(wordFrequencyList);
            List<WordFrequency> wordFrequencyCount = getWordValueAndCount(wordFrequencyListMap);
            List<WordFrequency> wordFrequencyListSored = sortWordFrequencyListByCount(wordFrequencyCount);
            return getString(wordFrequencyListSored);
        } catch (Exception e) {
            return ERROR_MESSAGE;
        }

    }

    private List<WordFrequency> sortWordFrequencyListByCount(List<WordFrequency> inputList) {
        inputList.sort((w1, w2) -> w2.getWordCount() - w1.getWordCount());
        return inputList;
    }

    private static String getString(List<WordFrequency> inputList) {
        StringJoiner joiner = new StringJoiner("\n");
        inputList.stream().forEach(wordFrequency -> {
            String text = wordFrequency.getValue() + " " + wordFrequency.getWordCount();
            joiner.add(text);
        });
        return joiner.toString();
    }

    private List<WordFrequency> getWordValueAndCount(Map<String, List<WordFrequency>> wordFrequencyListMap) {
        return wordFrequencyListMap.entrySet().stream().map(wordFrequency ->
                new WordFrequency(wordFrequency.getKey(),wordFrequency.getValue().size())).
                collect(Collectors.toList());
    }

    private static List<WordFrequency> getWordFrequencyList(String inputStr) {
        String[] wordFrequencyArr = inputStr.split("\\s+");
        return Arrays.stream(wordFrequencyArr).map(value ->
                new WordFrequency(value, INIT_COUNT)).collect(Collectors.toList());
    }


    private Map<String, List<WordFrequency>> getWordFrequencyListMap(List<WordFrequency> inputList) {
        Map<String, List<WordFrequency>> wordFrequencyListMap = new HashMap<>();
        for (WordFrequency input : inputList) {
            if (!wordFrequencyListMap.containsKey(input.getValue())) {
                ArrayList arr = new ArrayList<>();
                arr.add(input);
                wordFrequencyListMap.put(input.getValue(), arr);
            } else {
                wordFrequencyListMap.get(input.getValue()).add(input);
            }
        }
        return wordFrequencyListMap;
    }


}

### Objective
今天上午，我们首先以小组为单位进行了code review，然后进行了teamwork的展示。  
今天下午，我们学习了code refactor,主要学习了通过code smell识别需要重构的代码,  
包括过长的方法，无效的注释，魔法值，无意义的命名，重复的代码等，并通过两个  
案例，owingPrinter和wordFrequencyRefactor进行code refactor训练。其中,我印象最  
深刻的是进行wordFrequencyRefactor的重构练习的时候。

###  Reflective:
afflictive （难受的）

### Interpretive:
今天在进行wordFrequencyRefactor案例练习的时候，我发现这个需要被重构的代码  
读起来十分的晦涩难懂，从变量命名、方法抽离等方方面面都存在问题， 以至于我一  
开始在解读这份代码的时候花了不少时间，因此我觉得读这份代码使人十分难受。  

### Decisional:
通过今天的学习和练习，我深刻地感受到按照规范编写代码的重要性，这不仅方便我们  
理清思路，也方便同事阅读代码，更方便后期代码维护。因此，在今后的编码过程中我  
要时刻注意编码规范，重视代码的重构过程。
